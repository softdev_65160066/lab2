/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author Chanon
 */
public class Lab2 {

    static char[][] Table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int col, row;
    static char player = 'O';
    static int draw = 0;

    public static void showWelcome() {
        System.out.println("Welcome to OX game");
        System.out.println("trun O player");
    }

    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Table[i][j]);
            }
            System.out.println();
        }
    }

    public static void inputRC() {
        Scanner kb = new Scanner(System.in);
        int row = kb.nextInt();
        int col = kb.nextInt();
        Table[row][col] = player;
    }

    public static void switchPlayer() {
        if (player == 'O') {
            System.out.println("trun X player");
            player = 'X';
        } else {
            System.out.println("trun O player");
            player = 'O';
        }
    }

    public static boolean checkX() {
        for (int c = 0; c < 3; c++) {
            if (Table[c][0] == player && Table[c][1] == player && Table[c][2] == player) {
                return true;

            }

        }
        return false;
    }

    public static boolean checkY() {
        for (int r = 0; r < 3; r++) {
            if (Table[0][r] == player && Table[1][r] == player && Table[2][r] == player) {
                return true;

            }
        }
        return false;
    }

    public static boolean checkXY() {
        if (Table[0][0] == player && Table[1][1] == player && Table[2][2] == player) {
            return true;

        } else if (Table[0][2] == player && Table[1][1] == player && Table[2][0] == player) {
            return true;

        } else {
            return false;
        }

    }

    public static boolean checkDraw() {
        if (draw == 8) {
            return true;
        }

        return false;

    }

    public static boolean checkAll() {
        if (checkX()) {
            return true;
        }
        if (checkY()) {
            return true;
        }
        if (checkXY()) {
            return true;
        }
        return false;
    }
    
    
    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            inputRC();
            if (checkAll()) {
                showTable();
                System.out.println("win");
                break;

            } else if (checkDraw()) {
                showTable();
                System.out.println("Drow");
                break;
            }
            draw = draw + 1;
            switchPlayer();

        }

    }
}
